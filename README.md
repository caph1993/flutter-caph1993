# flutter-caph1993

This tool lets you easily install/uninstall the following list of tools in ubuntu based disttros without installing android studio (the big monster).

 + adb
 + gradle
 + dart
 + flutter
 + android command line tools
 + android sdk

Install/uninstall with:

 + `python3 installer.py install`
 + `python3 installer.py uninstall`

