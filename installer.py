from subprocess import run, PIPE, DEVNULL, Popen
import os, re, sys, getpass, json

#https://dev.to/jay_js/setting-up-flutter-without-android-studio-olo
#https://stackoverflow.com/questions/37505709/how-do-i-download-the-android-sdk-without-downloading-android-studio
#https://dart.dev/get-dart

HERE = os.path.dirname(os.path.abspath(__file__))


SUDO = None
def get_sudo():
    global SUDO
    if SUDO==None:
        run(['sudo', '-k'])
        pwd = getpass.getpass('Password for sudo: ')
        sudo = bytes(pwd+'\n', 'utf8')
        try:
            print('Verifying your password...',end='',flush=True)
            run(['sudo', '-S', 'true'], input=sudo, check=1, stdout=DEVNULL, stderr=DEVNULL, timeout=0.3)
            print('ok')
        except:
            print('Wrong password')
            sys.exit(1)
        SUDO = sudo
    return SUDO


PROFILE = None
def get_profile():
    if PROFILE==None:
        if os.environ.get('SHELL').endswith('/zsh'):
            PROFILE = os.path.expanduser('~/.bashrc')
        elif os.environ.get('SHELL').endswith('/bash'):
            PROFILE = os.path.expanduser('~/.zshrc')
        elif os.environ.get('SHELL').endswith('/sh'):
            PROFILE = os.path.expanduser('~/.profile')
        else:
            print('Sorry, your shell is not supported')
            exit(1)
    return PROFILE


ENV = {}
def source(text, uninstall=False):
    global ENV
    text = '\n'.join(l.lstrip() for l in text.split('\n'))
    text = text.strip()
    profile = get_profile()
    with open(profile, 'r') as f:
        contents = f.read()
    contents = contents.rstrip()+'\n'
    if not uninstall and text not in contents:
        contents = contents+pre+text+'\n'
    elif uninstall and text in contents:
        contents = contents.replace(text, '')
    with open(profile, 'w') as f:
        f.write(contents)
    p = shell(f'''
    . {profile} && python3 -c 'import os, json; print(json.dumps(dict(os.environ.items())))'
    ''', stdout=PIPE)
    ENV.update(json.loads(p.stdout.decode()))
    return


def split_commands(text):
    text = '\n'.join(l.strip() for l in text.split('\n'))
    text = '\n'.join(l for l in text.split('\n') if l)
    commands = []
    multiline = ''
    for cmd in text.split('\n'):
        if cmd.endswith('\\'):
            multiline += cmd
        else:
            commands.append(multiline+'\n'+cmd)
            multiline = ''
    if multiline:
        commands.append(multiline)
    return commands


def shell(text, **kwargs):
    p = None
    kwinput = kwargs.pop('input', None)
    for command in split_commands(text):
        cmd_input = kwinput
        if command.startswith('sudo'):
            command = 'sudo -S '+command[4:]
            cmd_input = get_sudo() + cmd_input
        p = run(
            command, shell=True, check=1,
            env={**os.environ, **ENV},
            input=cmd_input, **kwargs
        )
    return p


def works(command, **kwargs):
    print('Checking:', command, end='', flush=True)
    try:
        shell(command, stdout=DEVNULL, stderr=DEVNULL, **kwargs)
        print(' ...ok')
        return True
    except:
        print('\n')
        return False


def install_dart(uninstall=False):
    src = '''
    #dart
    export PATH="$PATH:/usr/lib/dart/bin"
    '''
    if uninstall:
        source(src, uninstall=True)
        shell('''
        sudo apt remove -y dart
        ''')  
    elif not works('dart --version'):
        shell('''
        sudo apt install apt-transport-https
        sudo sh -c 'wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'
        sudo sh -c 'wget -qO- https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
        sudo apt update
        sudo apt install -y dart
        ''')
        source(src)
    return


def install_flutter(uninstall=False):
    src = '''
    #flutter
    export PATH="$PATH:/usr/lib/flutter/bin"
    '''
    if uninstall:
        source(src, uninstall=True)
        shell('''
        sudo mkdir -p /usr/lib/flutter
        sudo rm -r /usr/lib/flutter
        ''')
    elif not works('flutter --version'):
        shell('''
        mkdir -p /tmp/caph1993
        git clone https://github.com/flutter/flutter.git /tmp/caph1993/flutter
        sudo mv /tmp/caph1993/flutter /usr/lib
        sudo chown $USER:$USER /usr/lib/flutter
        rm -r /tmp/caph1993
        ''')
        source(src)
        shell('''
        flutter precache
        flutter
        ''')
    return


def install_android(uninstall=False):
    src = '''
    #android
    export ANDROID_HOME="/usr/lib/Android"
    export PATH="$ANDROID_HOME/tools:$PATH"
    export PATH="$ANDROID_HOME/tools/bin:$PATH"
    export PATH="$ANDROID_HOME/platform-tools:$PATH"
    export PATH="$ANDROID_HOME/cmdline-tools/latest/bin:$PATH"
    export ANDROID_SDK_ROOT="/usr/lib/Android"
    export PATH="$ANDROID_SDK_ROOT:$PATH"
    '''

    if uninstall:
        source(src, uninstall=True)
        shell('''
        sudo apt remove -y adb
        sudo apt remove -y gradle
        ''')
        shell('''
        sudo mkdir -p /usr/lib/Android
        sudo rm -r /usr/lib/Android
        ''')
    else:
        if not works('adb --version') or not works('gradle --version'):
            shell('''
            sudo apt update
            sudo apt install -y adb
            sudo apt install -y gradle
            ''')
        if not works('sdkmanager --version'):
            shell('''
            mkdir -p /tmp/caph1993
            curl https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip > /tmp/caph1993/cmdline-tools.zip
            unzip -uo /tmp/caph1993/cmdline-tools.zip -d /tmp/caph1993
            mkdir -p /tmp/caph1993/Android
            mkdir -p /tmp/caph1993/Android/cmdline-tools
            mv /tmp/caph1993/tools /tmp/caph1993/Android/cmdline-tools/latest
            sudo cp -r /tmp/caph1993/Android /usr/lib/Android
            sudo rm -r /tmp/caph1993
            ''')
        shell('''
        sudo chown $USER:$USER /usr/lib/Android
        ''')
        source(src)

        latest = get_latest_sdkmanager_versions()
        yes100 = bytes('y\n'*100, 'utf8') # Accept all terms and conditions
        shell('''
        sdkmanager "system-images;android-{v_android};google_apis;x86_64"
        sdkmanager "platforms;android-{v_android}"
        sdkmanager "patcher;{v_patcher}"
        sdkmanager "emulator"
        sdkmanager "build-tools;{v_buildtools}"
        sdkmanager --update
        '''.format(**latest), input=yes100)
    return

def get_latest_sdkmanager_versions():
    p = shell('sdkmanager --list', stdout=PIPE)
    out = p.stdout.decode()
    latest = {
        'v_android': r'system-images;android-(\d+);',
        'v_buildtools': r'build-tools;(\d+.\d+.\d+)[; $]',
        'v_patcher': r'patcher;(v\d+)[; $]',
    }
    def version_split(s):
        l = re.split(r'(\d+)', s)
        return [int(x) if x.isdigit() else x for x in l]
    for k, v in latest.items():
        latest[k] = max(re.findall(v, out), key=version_split)
    return latest


def install():
    install_dart()
    install_flutter()
    install_android()
    yes100 = bytes('y\n'*100, 'utf8') # Accept all terms and conditions
    shell(r'''
    flutter doctor --android-licenses
    ''', input=yes100)
    return

def uninstall():
    install_dart(uninstall=True)
    install_flutter(uninstall=True)
    install_android(uninstall=True)
    return
     

if __name__=='__main__':
    _, *args =  sys.argv
    if  args!=['install'] and args!=['uninstall']:
        print(
            'Run one of'
            '\n    python3 installer.py install'
            '\n    python3 installer.py uninstall'
        )
        sys.exit(1)
    assert os.getuid()!=0, 'Please run as user'
    get_sudo()
    source('')
    if args==['install']:
        install()
        if works('sdkmanager --version', env=None):
            print('\n---\nFinished')
        else:
            print('\n---\nFinished.')
            print('Restart is required to reload', get_profile())
    else:
        uninstall()
        print('\n---\nFinished')
